package jp.yorihime.BTTransrater;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;

/**
 * <p>
 * �ｽy�ｽA�ｽ�ｽ�ｽ�ｽ�ｽO�ｽ�ｽ�ｽﾄゑｿｽ�ｽ�ｽZeal�ｽﾆ接托ｿｽ�ｽ�ｽ�ｽ�ｽB<br>
 * </p>
 * 
 * @author:�ｽﾃ撰ｿｽ@�ｽ�ｽ
 * @since:2010/12/14
 * @category:Thread
 */
public class BluetoothThread extends Thread {
	private BluetoothSocket bs;
	private InputStream mmInStream;
	private OutputStream mmOutStream;
	private Handler mHandler; // �ｽ�ｽ�ｽC�ｽ�ｽ�ｽX�ｽ�ｽ�ｽb�ｽh�ｽﾖの�ｿｽ�ｽb�ｽZ�ｽ[�ｽW�ｽn�ｽ�ｽ�ｽh�ｽ�ｽ
	private boolean life = true;

	private final int MASK_UUID16 = 0x0000FFFF;
	private final int UUID16 = 0x1101;
	private final String UUID_BASE = "0000-1000-8000-00805F9B34FB";
	public final static int MAX_DEVICES = 254; // �ｽﾅ托ｿｽC�ｽ�ｽ�ｽf�ｽb�ｽN�ｽX�ｽ�ｽ

	// �ｽ�ｽ�ｽb�ｽZ�ｽ[�ｽW�ｽﾖ連
	public static final int CONNECT = 1; // �ｽﾚ托ｿｽ�ｽ�ｽ�ｽ�ｽ
	public static final int UNCONNECT = 2; // �ｽﾚ托ｿｽ�ｽ�ｽ�ｽs
	public static final int COMMAND = 3; // �ｽ�ｽM�ｽR�ｽ}�ｽ�ｽ�ｽh
	public static final int CONNECTIONLOST = 4; // �ｽ^�ｽC�ｽ�ｽ�ｽA�ｽE�ｽg

	BluetoothThread(BluetoothDevice device, Handler handler) {
		this.mHandler = handler;
		try {
			bs = device.createRfcommSocketToServiceRecord(getUUID());
			bs.connect();
			this.mmInStream = bs.getInputStream();
			this.mmOutStream = bs.getOutputStream();
		} catch (IOException e) {
			Close();
		}
	}

	/**
	 * <p>
	 * �ｽo�ｽb�ｽt�ｽ@�ｽﾉ趣ｿｽM�ｽf�ｽ[�ｽ^�ｽ�ｽ�ｽ�ｽ�ｽ�ｽ鼾�ｿｽA�ｽ�ｽ�ｽC�ｽ�ｽ�ｽX�ｽ�ｽ�ｽb�ｽh�
	 * ｽﾉ托ｿｽ�ｽ�ｽB
	 * </p>
	 */
	public void run() {
		byte[] buffer = new byte[128];
		int bytes;
		if (life) {
			// �ｽﾚ托ｿｽ�ｽ�ｽ�ｽ�ｽ			
			mHandler.obtainMessage(CONNECT).sendToTarget();
		} else {
			// �ｽﾚ托ｿｽ�ｽ�ｽ�ｽs
			mHandler.obtainMessage(UNCONNECT).sendToTarget();
		}
		while (life) {
			try {
				try {
					// �ｽo�ｽb�ｽt�ｽ@�ｽﾌデ�ｽ[�ｽ^�ｽﾆ抵ｿｽ�ｽ�ｽ�ｽ�ｽ�ｽ謫ｾ
					bytes = mmInStream.read(buffer);
					// 解析
					if (buffer[0] == 0x11
							&& bytes == ((ByteToInt(buffer[2]) * 256 + ByteToInt(buffer[3]))) + 4) {
						mHandler.obtainMessage(COMMAND, bytes, 0, buffer)
								.sendToTarget();
					}
				} catch (NullPointerException e) {
					break;
				}
			} catch (IOException e) {
				mHandler.obtainMessage(CONNECTIONLOST).sendToTarget();
				Close();
				break;
			}
		}
	}

	/**
	 * <p>
	 * �ｽo�ｽC�ｽg�ｽf�ｽ[�ｽ^�ｽ乱M�ｽ�ｽ�ｽ�ｽB
	 * </p>
	 */
	public void SEND(byte[] SendCommand) { // �ｽ�ｽ�ｽZ�ｽb�ｽg�ｽ�ｽ
		try {
			mmOutStream.write(SendCommand);
		} catch (IOException e) {
			// �ｽﾘ断�ｽ�ｽ�ｽ黷ｽ�ｽ�ｽ�ｽﾆゑｿｽﾊ知�ｽ�ｽ�ｽ�ｽ
			mHandler.obtainMessage(CONNECTIONLOST).sendToTarget();
			Close();
		}
	}

	public void Close() {
		life = false;
		try {
			bs.close();
		} catch (IOException e) {
		}
	}

	public UUID getUUID() {
		StringBuilder b = new StringBuilder();
		String hex = Integer.toHexString(UUID16 & MASK_UUID16);
		b.append("00000000".substring(hex.length()));
		b.append(hex);
		b.append('-');
		b.append(UUID_BASE);
		UUID uuid = UUID.fromString(b.toString());
		return uuid;
	}

	private int ByteToInt(byte data) {
		int result = 0;
		if (data < 0) {
			result = 256 - data;
		} else {
			result = data;
		}
		return result;
	}
}