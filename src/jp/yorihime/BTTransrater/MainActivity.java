package jp.yorihime.BTTransrater;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Set;
import java.util.Vector;

import jp.yorihime.BTTransrater.R.id;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	BluetoothThread btt;
	TextView tv;
	TextView mode;
	ImageButton btcall;
	ImageButton btahead;
	ImageButton btstop;
	ImageButton btright;
	ImageButton btleft;
	ImageButton btback;
	ImageButton btdemo;

	private static final int REQUEST_CODE = 0;
	private static final int MAX_MSGCOUNT = 5;
	private static final int START = 0xFF;

	private final byte ST = 0x11;
	private final byte NONE = 0x00;
	private final byte START_REQ = 0x40; // 開始
	private final byte START_RES = 0x41; // 開始
	private final byte STOP_REQ = 0x50; // 終了
	private final byte STOP_RES = 0x51; // 終了
	private final byte MSG_SUCSESS = 0x60; // メッセージ
	private final byte MSG_ERROR = 0x61; // メッセージ
	private final byte MOTOR_AHEAD = 0x70; // 前
	private final byte MOTOR_RIGHT = 0x71; // 右
	private final byte MOTOR_LEFT = 0x72; // 左
	private final byte MOTOR_STOP = 0x73; // 停止
	private final byte MOTOR_BACK = 0x74; // 後退
	private final byte MOTOR_DEMO = 0x75; // デモ
	private boolean transrate = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// アイコン表示を有効にする
	    requestWindowFeature(Window.FEATURE_LEFT_ICON);
	    
		setContentView(R.layout.activity);
		
	    // アイコン表示
	    setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);

		tv = (TextView) findViewById(id.Text);
		mode = (TextView) findViewById(id.Mode);
		btcall = (ImageButton) findViewById(id.Call);
		btahead = (ImageButton) findViewById(id.Ahead);
		btstop = (ImageButton) findViewById(id.Stop);
		btright = (ImageButton) findViewById(id.Right);
		btleft = (ImageButton) findViewById(id.Left);
		btback = (ImageButton) findViewById(id.Back);
		btdemo = (ImageButton) findViewById(id.Demo);
		
		btcall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				setCallView();
			}
		});
		
		btahead.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				btt.SEND(makeCommand(MOTOR_RIGHT));
			}
		});
		
		btstop.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				btt.SEND(makeCommand(MOTOR_STOP));
			}
		});
		
		btright.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				btt.SEND(makeCommand(MOTOR_LEFT));;
			}
		});
		
		btleft.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				btt.SEND(makeCommand(MOTOR_BACK));
			}
		});
		
		btback.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				btt.SEND(makeCommand(MOTOR_AHEAD));
			}
		});
		
		btdemo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO 自動生成されたメソッド・スタブ
				btt.SEND(makeCommand(MOTOR_DEMO));
			}
		});

		new Thread(new Runnable() {
			@Override
			public void run() {
				BluetoothAdapter bluetooth_adapter = BluetoothAdapter
						.getDefaultAdapter();
				if (!bluetooth_adapter.isEnabled()) // �ｽ�ｽ�ｽ�ｽBluetooth�ｽ�ｽOFF�ｽﾉなゑｿｽ�ｽﾄゑｿｽ�ｽ�ｽ�ｽ�ｽON�ｽﾉゑｿｽ�ｽ�ｽ謔､�ｽﾉゑｿｽ�ｽ�ｽ
				{
					Intent enable_bt = new Intent(
							BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(enable_bt, 1);
				}
				// �ｽ�ｽ�ｽ�ｽBluetooth�ｽ�ｽON�ｽﾉ最擾ｿｽ�ｽ�ｽ�ｽ�ｽﾈゑｿｽ�ｽﾄゑｿｽ�ｽ�ｽ�ｽ�ｽ
				else {
					BluetoothDevice dev;
					Vector<BluetoothDevice> devices = new Vector<BluetoothDevice>();

					// getBoundedDevices�ｽﾉゑｿｽ�ｽ�ｽﾄピ�ｽA�ｽ�ｽ�ｽ�ｽ�ｽO�ｽ�ｽ�ｽ�ｽﾉなゑｿｽ�ｽ�ｽﾄゑｿｽ�ｽ�ｽf�ｽo�ｽC�ｽX�ｽ�ｽ�ｽ謫ｾ�ｽ�ｽ�ｽ�ｽ
					Set<BluetoothDevice> paired_devices = bluetooth_adapter
							.getBondedDevices();

					// BluetoothDevice�ｽﾌイ�ｽ�ｽ�ｽX�ｽ^�ｽ�ｽ�ｽX�ｽ�ｽdevices�ｽC�ｽ�ｽ�ｽX�ｽ^�ｽ�ｽ�ｽX�ｽﾉ追会ｿｽ�ｽ�ｽ�ｽﾄゑｿｽ�ｽ�ｽ
					for (BluetoothDevice device : paired_devices) {
						devices.add(device);
					}
					dev = devices.elementAt(0);
					if (!devices.isEmpty()) {
						btt = null;
						btt = new BluetoothThread(dev, mHandler);
						btt.start();
					}
				}
			}
		}).start();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (btt != null) {
			btt.checkAccess();
		}
	}

	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case BluetoothThread.CONNECT:
				mode.setText("接続しました");
				btcall.setEnabled(true);
				btahead.setEnabled(true);
				btstop.setEnabled(true);
				btright.setEnabled(true);
				btleft.setEnabled(true);
				btback.setEnabled(true);
				btdemo.setEnabled(true);
				break;
			case BluetoothThread.UNCONNECT:
				Toast.makeText(MainActivity.this, "接続に失敗しました",
						Toast.LENGTH_LONG).show();
				finish();
				break;
			case BluetoothThread.CONNECTIONLOST:
				Toast.makeText(MainActivity.this, "接続が解除されました",
						Toast.LENGTH_LONG).show();
				finish();
				break;
			case BluetoothThread.COMMAND:
				byte[] data = (byte[]) msg.obj;
				switch (data[1]) {
				case START_REQ:
					setCallView();
					break;
				case STOP_REQ:
					if (transrate) {
						finish();
						transrate = false;
					}
					break;
				}
				break;
			}
		}
	};
	
	private void setCallView(){
		tv.setText("");
		try {
			// インテント作成
			Intent intent = new Intent(
					RecognizerIntent.ACTION_RECOGNIZE_SPEECH); // ACTION_WEB_SEARCH
			intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
					RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
					"話しかけてください"); // お好きな文字に変更できます

			// インテント発行
			startActivityForResult(intent, REQUEST_CODE);
			transrate = true;
		} catch (ActivityNotFoundException e) {
			// このインテントに応答できるアクティビティがインストールされていない場合
			Toast.makeText(MainActivity.this,
					"音声認識ソフトがインストールされていません", Toast.LENGTH_LONG)
					.show();
			btt.SEND(makeCommand(MSG_ERROR));
			finish();
		}
	}

	// アクティビティ終了時に呼び出される
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 自分が投げたインテントであれば応答する
		if (requestCode == REQUEST_CODE && transrate) {
			transrate = false;
			if (resultCode == RESULT_OK) {
				String resultsString = "";
				int count = 0;

				try {
					tv.setText("");

					// 結果文字列リスト
					ArrayList<String> results = data
							.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

					// 5つまでの結果を送信
					count = results.size();
					if (count > MAX_MSGCOUNT) {
						count = MAX_MSGCOUNT;
					}
					
					tv.setText("音声認識結果");

					// 文字列連結
					for (int i = 0; i < count; i++) {
						tv.setText(tv.getText() + "\n" + results.get(i));
						resultsString += results.get(i) + ",";
					}

					// 送信
					btt.SEND(makeCommand(resultsString));

				} catch (Exception e) {
					// トーストを使って結果を表示
					Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG)
							.show();
				}
			} else {
				btt.SEND(makeCommand(MSG_ERROR));
				tv.setText("音声認識エラー");
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private byte[] makeCommand(byte type) {
		byte[] data = new byte[4];
		data[0] = ST;
		data[1] = type;
		data[2] = 0;
		data[3] = 0;
		return data;
	}

	private byte[] makeCommand(String str) throws UnsupportedEncodingException {
		byte[] msg = str.getBytes("SJIS");
		byte[] data = new byte[4 + msg.length];
		data[0] = ST;
		data[1] = MSG_SUCSESS;
		data[2] = IntToByte((msg.length >> 8) & (0x00FF));
		data[3] = IntToByte(msg.length & 0x00FF);
		for (int Loop = 0; Loop < msg.length; Loop++) {
			data[4 + Loop] = msg[Loop];
		}
		return data;
	}

	private byte IntToByte(int data) {
		byte result = 0;
		if (data > 0x80) {
			result = (byte) (data - 256);
		} else {
			result = (byte) data;
		}
		return result;
	}
}
