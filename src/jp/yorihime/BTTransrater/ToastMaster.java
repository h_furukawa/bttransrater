package jp.yorihime.BTTransrater;

import android.content.Context;
import android.widget.Toast;

/**
 * <p>
 * Toast�Ɋւ���Class<br>
 * Toast�͓����ɂQ�N�����Ă͂����Ȃ����߁A<br>
 * �}���`�X���b�h�������v���O������ł̑��d�N����h�����������B
 * </p>
 * 
 * @author:�Ð�@��
 * @since:2010/12/7
 * @category:Class
 */
public class ToastMaster extends Toast {
	private static Toast sToast = null;

	public ToastMaster(Context context) {
		super(context);
	}

	@Override
	public void show() {
		ToastMaster.setToast(this);
		super.show();
	}

	public static void setToast(Toast toast) {
		if (sToast != null)
			sToast.cancel();
		sToast = toast;
	}

	public static void cancelToast() {
		if (sToast != null)
			sToast.cancel();
		sToast = null;
	}
}
